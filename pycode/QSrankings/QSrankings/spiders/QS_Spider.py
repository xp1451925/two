import scrapy
from scrapy import Request
import sys
import os
from bs4 import BeautifulSoup

fpath = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
ffpath = os.path.abspath(os.path.join(fpath,".."))
print(ffpath)
sys.path.append(ffpath)
from QSrankings.items import QsItem

class QsSpiderSpider(scrapy.Spider):
    name = "QS_Spider"
    allowed_domains = ["http://www.betteredu.net/"]
    # start_urls = ['http://rankings.betteredu.net/qs/world-university-rankings/latest/2019-2020.html']
    ## 获取指定链接，在列表中添加年份可以获取对应年份的数据
    def start_requests(self):
        url_list = [2019,2020,2021]
        request_header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0"
        }         
        for i in url_list:
            start_urls = f"http://rankings.betteredu.net/qs/world-university-rankings/latest/{i-1}-{i}.html"
            yield Request(url=start_urls, headers=request_header, dont_filter=True)
    ## 解析函数，使用BeautifulSoup方法检索网页源码下所有在class=rks_qsshowct之后的所有<tr>标签，其中对应着不同数据行
    def parse(self, response):
            html_doc = response.body
            soup = BeautifulSoup(html_doc, 'html.parser')
            tr_list = soup.select('.rks_qsshowct tr')
            for i in range(1, 500):
                item =QsItem()
                tr = tr_list[i]
                td_list = tr.select('td')
                item['rank'] = td_list[0].text.strip()
                item['university'] = td_list[1].text.strip()
                item['country_name'] = td_list[2].text.strip()
                item['overall_score'] = td_list[3].text.strip()
                item['academic_reputation'] = td_list[4].text.strip()
                item['employer_reputation'] = td_list[5].text.strip()
                item['faculty_student'] = td_list[6].text.strip()
                item['international_faculty'] = td_list[7].text.strip()
                item['international_students'] = td_list[8].text.strip()
                item['citations_per_faculty'] = td_list[9].text.strip()
                yield item
            
if __name__ == '__main__':
    from scrapy import cmdline
    cmdline.execute("scrapy crawl QS_Spider -o test.csv".split())