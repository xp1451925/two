# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class QsItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    rank = scrapy.Field()
    university = scrapy.Field()
    country_name = scrapy.Field()
    overall_score = scrapy.Field()
    academic_reputation = scrapy.Field()
    employer_reputation = scrapy.Field()
    faculty_student = scrapy.Field()
    international_faculty = scrapy.Field()
    international_students = scrapy.Field()
    citations_per_faculty = scrapy.Field()


