# 导入所需库
from sklearn.datasets import load_files
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

# 加载IMDB电影评论数据集
reviews = load_files('test', categories=['neg', 'pos'], shuffle=True)

# 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(reviews.data, reviews.target, test_size=0.2, random_state=42)

# 特征提取：将文本转换为TF-IDF特征向量
vectorizer = TfidfVectorizer(max_features=6000)
X_train = vectorizer.fit_transform(X_train).toarray()
X_test = vectorizer.transform(X_test).toarray()

# 创建并训练逻辑回归模型
classifier = LogisticRegression()
classifier.fit(X_train, y_train)

# 在测试集上进行预测
predictions = classifier.predict(X_test)

# 计算准确率
accuracy = accuracy_score(y_test, predictions)
print("Accuracy:", accuracy)