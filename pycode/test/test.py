import requests
from bs4 import BeautifulSoup
 
headers = {
    "user-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0"
}
start_urls = 'http://rankings.betteredu.net/qs/world-university-rankings/latest/2020-2021.html'
# url_list = [2019,2020,2021]
# for i in url_list:
#         start_urls = f"http://rankings.betteredu.net/qs/world-university-rankings/latest/{i-1}-{i}.html"
response = requests.get(start_urls)
soup = BeautifulSoup(response.content, 'html.parser')
 
tr_list = soup.select('.rks_qsshowct tr')
# f = open('test.csv', 'w', newline='', encoding='utf8')
f = open('qs.txt', 'w', encoding='utf-8')
for i in range(1, len(tr_list) - 1):
    tr = tr_list[i]
    td_list = tr.select('td')
    name = td_list[0].text.strip()
    birthTime = td_list[1].text.strip()
    height = td_list[2].text.strip()
    smash = td_list[3].text.strip()
    block = td_list[4].text.strip()
    position = td_list[5].text.strip()
    province = td_list[6].text.strip()
    province1 = td_list[7].text.strip()
    province2 = td_list[8].text.strip()
    province3 = td_list[9].text.strip()
    # Qs_item['rank'] = Qs_element.xpath('.//tr/td[1]/text()').get()
    # Qs_item['university'] = Qs_element.xpath('.//tr/td[2]/text()').get()
    # Qs_item['country_name'] = Qs_element.xpath('.//tr/td[3]/text()').get()
    # Qs_item['overall_score'] = Qs_element.xpath('.//tr/td[4]/text()').get()
    # Qs_item['academic_reputation'] = Qs_element.xpath('.//tr/td[5]/text()').get()
    # Qs_item['employer_reputation'] = Qs_element.xpath('.//tr/td[6]/text()').get()
    # Qs_item['faculty_student'] = Qs_element.xpath('.//tr/td[7]/text()').get()
    # Qs_item['international_faculty'] = Qs_element.xpath('.//tr/td[8]/text()').get()
    # Qs_item['international_students'] = Qs_element.xpath('.//tr/td[9]/text()').get()
    # Qs_item['citations_per_faculty'] = Qs_element.xpath('.//tr/td[10]/text()').get()

 
    f.write(name + ",")
    f.write(birthTime + ",")
    f.write(height + ",")
    f.write(smash + ",")
    f.write(block + ",")
    f.write(position + ",")
    f.write(province + ",")
    f.write(province1 + ",")
    f.write(province2 + ",")
    f.write(province3 + '\n')
 
f.close()
print("数据获取完成")