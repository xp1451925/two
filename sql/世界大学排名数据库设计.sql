create database if not exists QS_WUR default charset utf8 collate utf8_general_ci;
use qs_wur;

-- 综合排名
create table qs_ranking(
	id int not null auto_increment comment '排名',
	university varchar(200) not null comment '学校名称',
	location varchar(20) not null comment '地区',
	OverallScore decimal(4, 1) not null comment '综合评分',
	primary key (`id`))
engine = InnoDB
default character set = utf8;
select * from qs_ranking;
select count(*) from qs_ranking;

-- 地区
create table location(
	location varchar(200) not null,
	university varchar(200) not null,
	primary key (`university`))
engine = InnoDB
default character set = utf8;
alter table location add id int;
alter table location add foreign key (id) references qs_ranking (id);
select * from location;

-- 美国
create table US_ranking(
	id int not null auto_increment comment '排名',
	university varchar(200) not null comment '学校名称',
    location varchar(20) not null comment '地区',
	OverallScore decimal(4, 1) not null comment '综合评分',
	AcademicReputation decimal(4, 1) not null comment '学术声誉',
	EmployerReputation decimal(4, 1) not null comment '雇主声誉',
	FacultyStudentRatio decimal(4, 1) not null comment '师生占比',
	InternationalStudentRatio decimal(4, 1) not null comment '国际学生占比',
	InternationalFacultyRatio decimal(4, 1) not null comment '国际教师占比',
	CitationsPerFaculty decimal(4, 1) not null comment '教员引用率',
	primary key (`id`),
    constraint `FK_location_us_ranking`
	foreign key (`university`)
    references location (`university`))
engine = InnoDB
default character set = utf8;
select * from us_ranking where OverallScore > 80 and InternationalStudentRatio >= 90;

-- 英国
create table Uk_ranking(
	id int not null auto_increment comment '排名',
	university varchar(200) not null comment '学校名称',
    location varchar(20) not null comment '地区',
	OverallScore decimal(4, 1) not null comment '综合评分',
	AcademicReputation decimal(4, 1) not null comment '学术声誉',
	EmployerReputation decimal(4, 1) not null comment '雇主声誉',
	FacultyStudentRatio decimal(4, 1) not null comment '师生占比',
	InternationalStudentRatio decimal(4, 1) not null comment '国际学生占比',
	InternationalFacultyRatio decimal(4, 1) not null comment '国际教师占比',
	CitationsPerFaculty decimal(4, 1) not null comment '教员引用率',
	primary key (`id`),
    constraint `FK_location_uk_ranking`
	foreign key (`university`)
    references location (`university`))
engine = InnoDB
default character set = utf8;
select * from uk_ranking;
-- 日本
create table japan_ranking(
	id int not null auto_increment comment '排名',
	university varchar(200) not null comment '学校名称',
    location varchar(20) not null comment '地区',
	OverallScore decimal(4, 1) not null comment '综合评分',
	AcademicReputation decimal(4, 1) not null comment '学术声誉',
	EmployerReputation decimal(4, 1) not null comment '雇主声誉',
	FacultyStudentRatio decimal(4, 1) not null comment '师生占比',
	InternationalStudentRatio decimal(4, 1) not null comment '国际学生占比',
	InternationalFacultyRatio decimal(4, 1) not null comment '国际教师占比',
	CitationsPerFaculty decimal(4, 1) not null comment '教员引用率',
	primary key (`id`),
    constraint `FK_location_japan_ranking`
	foreign key (`university`)
    references location (`university`))
engine = InnoDB
default character set = utf8;
select * from japan_ranking;
-- 中国
create table cn_ranking(
	id int not null auto_increment comment '排名',
	university varchar(200) not null comment '学校名称',
    location varchar(20) not null comment '地区',
	OverallScore decimal(4, 1) not null comment '综合评分',
	AcademicReputation decimal(4, 1) not null comment '学术声誉',
	EmployerReputation decimal(4, 1) not null comment '雇主声誉',
	FacultyStudentRatio decimal(4, 1) not null comment '师生占比',
	InternationalStudentRatio decimal(4, 1) not null comment '国际学生占比',
	InternationalFacultyRatio decimal(4, 1) not null comment '国际教师占比',
	CitationsPerFaculty decimal(4, 1) not null comment '教员引用率',
	primary key (`id`),
    constraint `FK_location_cn_ranking`
	foreign key (`university`)
    references location (`university`))
engine = InnoDB
default character set = utf8;
select * from cn_ranking;
-- 澳洲
create table australia_ranking(
	id int not null auto_increment comment '排名 --PK',
	university varchar(200) not null comment '学校名称',
    location varchar(20) not null comment '地区',
	OverallScore decimal(4, 1) not null comment '综合评分',
	AcademicReputation decimal(4, 1) not null comment '学术声誉',
	EmployerReputation decimal(4, 1) not null comment '雇主声誉',
	FacultyStudentRatio decimal(4, 1) not null comment '师生占比',
	InternationalStudentRatio decimal(4, 1) not null comment '国际学生占比',
	InternationalFacultyRatio decimal(4, 1) not null comment '国际教师占比',
	CitationsPerFaculty decimal(4, 1) not null comment '教员引用率',
	primary key (`id`),
    constraint `FK_location_australia_ranking`
	foreign key (`university`)
    references location (`university`))
engine = InnoDB
default character set = utf8;
select * from us_ranking;


-- 声誉
create table reputation(
	id int not null,
	university varchar(200) not null comment '学校名称',
	location varchar(20) not null comment '地区',
    AcademicReputation decimal(4, 1) not null comment '学术声誉',
	EmployerReputation decimal(4, 1) not null comment '雇主声誉',
    primary key (`id`),
    constraint `FK_location_reputation`
    foreign key (`university`)
    references qs_wur.location (`university`))
engine = InnoDB
default character set = utf8;
select avg(AcademicReputation) from reputation;
select * from reputation where AcademicReputation > (select avg(AcademicReputation) from reputation);

-- 占比
create table ratio(
	id int not null,
	university varchar(200) not null comment '学校名称',
    location varchar(20) not null comment '地区',
    FacultyStudentRatio decimal(4, 1) not null comment '师生占比',
	InternationalStudentRatio decimal(4, 1) not null comment '国际学生占比',
	InternationalFacultyRatio decimal(4, 1) not null comment '国际教师占比',
    primary key (`id`),
    constraint `FK_location_ratio`
    foreign key (`university`)
    references qs_wur.location (`university`))
engine = InnoDB
default character set = utf8;
select * from ratio where (InternationalStudentRatio between 40 and 50) 
and location='united kingdom' order by FacultyStudentRatio desc;

-- 用户需求
create table employee_favor(
	employeeID varchar(20) not null comment '用户id --PK',
	employee_name varchar(20) not null comment '用户名称',
    location varchar(20) not null comment '期望国家',
    score decimal(4, 1) not null comment '期望综合评分',
    AcademicReputation decimal(4, 1) not null comment '期望学术声誉',
    InternationalStudentRatio decimal(4, 1) not null comment '期望国际生占比',
    primary key (`employeeID`))
    
engine = InnoDB
default character set = utf8;
-- alter table employee_favor add foreign key(location) references qs_wur.location(location);
-- alter table employee_favor drop foreign key location;
select * from employee_favor;

-- select employee_favor.*, location.location, reputation.AcademicReputation, ratio.InternationalStudentRatio from employee_favor inner join location 
-- on employee_favor.location=location.location inner join reputation
-- on employee_favor.AcademicReputation=reputation.AcademicReputation inner join ratio
-- on employee_favor.InternationalStudentRatio=ratio.InternationalStudentRatio;


INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230601','张三丰','United States',90.3,95.5,75.7);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230602','李晓六','United Kingdom',81.6,99.3,66.4);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230603','华旺卿','Japan',78.4,74.6,15.5);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230604','王伞','China',72,81.8,15.6);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230605','李好','United States',98.7,95.4,65.2);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230606','张佳乐','United Kingdom',99.9,94.6,99.7);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230607','赤木洋子','China',89.3,81.8,39.2);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230608','平井桃','Japan',84,79.4,21);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230609','简自豪','China',87.5,53.7,45.1);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230610','陶喆','United States',92,85,14.3);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230611','梁龙','Japan',88.7,68.7,21.5);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230612','雷龙','United States',78.6,54.8,20.8);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio)
VALUES('20230613','金多贤','China',96,97.4,30.1);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230614','余佳运','Japan',84.2,98.7,11.1);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230615','施耐庵','Australia',87.3,91.2,99.5);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230616','张辽','United Kingdom',100,76.6,91.2);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230617','YouKnowWho','Australia',93,88,86.9);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230618','鲤','China',94.3,79.6,20.1);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230619','河蚌三','United Kingdom',41.4,61.3,97.9);
INSERT INTO employee_favor(employeeID,employee_name,location,score,AcademicReputation,InternationalStudentRatio) 
VALUES('20230620','王铁柱','Australia',78.3,62.4,59.2);

-- 疫情发生前后的大学排名变化
create table recent_change(
	country  varchar(20) not null auto_increment,
    before_pandemic int not null comment '2018-2019排名',
    pandemic_start int not null comment '2019-2020排名',
    during_pandemic int not null comment '2020-2021排名',
    primary key (`country`)
    )
engine = InnoDB
default character set = utf8;
select * from recent_change order by before_pandemic desc;

